function goto_yerrampe() {
    $("#landing_bg, #landing_links").animate(
        { opacity: 0},
        1000,
        function(){$('#landing_box').animate({ height: 0 }, 800) }
    ); // end first animate(

    $('#searcher').focus();
}

function view_headword(target_headword) {
    var url = 'headword/' + target_headword + '/index.html';

    $("#wordviewer").attr('src', url);

    if($('#mob_back').is(':visible')) {
        $('body').animate({
            scrollTop: 0,
            scrollLeft: $(document).width()
        }, 500);
    }
}

$('#searcher_clearer').click(function() {
    $("#searcher").val('');
    $('#wordlist').html('');
});

$('#mob_back').click(function() {
    $('body').animate({
        scrollTop: 0,
        scrollLeft: 0
    }, 250);
});

var delay = (function(){
    var timer = 0;
    return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
    };
})();

function listWords() {
    var search = $('#searcher').val();

    var options = {
      pre: '<strong>'
    , post: '</strong>'
    , extract: function(el) { return el.search_str }
  }

  if(search != '') {
    // var search_object = yerrampe_index.map(function(headword) {
    //     return({
    //         "lx_str": headword,
    //         "search_str": headword.replace(/ \_.+/, "")
    //     });
    // });

    var filtered = fuzzy.filter(search, yerrampe_index, options);

    var newWords = filtered.map(function(el) {
        var html_str = el.string;

        hm = el.original.lx_str.match(/([0-9])$/);

        if(hm != null) {
            html_str += "<sup>" + hm[0] + "</sup>";
        }

        return '<li class="wordinlist" data-headword="' + el.original.lx_str + '">' + html_str + '</li>';
    });

    $("#wordlist").html(newWords.join(''));

  } else {
    $("#wordlist").empty();
  }
}

$("#wordlist").delegate("li.wordinlist", "click", function() {
    var target_headword = $(this).attr('data-headword');

    view_headword(target_headword);
});

$("#searcher").keyup(function() {
    delay(function() {
        $("#wordlist_box").scrollTop(0);

        listWords();

    }, 250); // delay the trigger to 500 ms after user stops typing
});

$('#search_form').submit(function() {
    first_word = $('#wordlist').children().first();

    if(first_word.length != 0) {
        view_headword(first_word.attr('data-headword'));
    }

    return(false);
});
