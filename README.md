# Hugo theme for Yerrampe (web dictionary for the Kaytetye language)

To render the web dictionary for Kaytetye, we use the [Hugo](https://gohugo.io/) static site generator framework with this custom theme. The data for individual words in the dictionary are injected as individual 'posts' (e.g. `word1.md`, `word2.md`), which are rendered into HTML pages by Hugo. The aim is for the rendered site to be as low-tech as possible and can run on relatively old PC computers in the schools (i.e. Internet Explorer compatible, as seen in the photo below).

![_DSF3182](https://user-images.githubusercontent.com/9938298/216672682-2912c341-d7d5-45a9-8afe-baf04f86043a.jpg)

## Screenshots

<img width="1346" alt="Screenshot 2023-02-03 at 9 39 56 AM" src="https://user-images.githubusercontent.com/9938298/216672348-02f4f4a9-4cf8-4b43-aec1-262abe85867a.png">
<img width="1346" alt="Screenshot 2023-02-03 at 9 40 06 AM" src="https://user-images.githubusercontent.com/9938298/216672334-776f2ca7-0009-4349-a842-70ef8bd1c816.png">
